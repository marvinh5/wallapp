from rest_framework import serializers
from wall.models import Posts, Comments
from rest_framework.pagination import PageNumberPagination

class PostSerializer(serializers.ModelSerializer):
    likes = serializers.IntegerField(read_only=True)
    class Meta:
        model = Posts
        fields = ('id', 'title', 'content', 'author', 'created', 'likes')


class CommentSerializer(serializers.ModelSerializer):
    likes = serializers.IntegerField(read_only=True)
    class Meta:
        model= Comments
        fields = ('id', 'author', 'content', 'post' , 'likes')


class PaginatedPostSerializer(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000