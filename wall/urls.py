from django.conf.urls import url
from wall import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^posts/(?P<postId>[0-9]+)/like$', views.LikePost.as_view()),
    url(r'^posts$', views.PostList.as_view()),
    url(r'^posts/(?P<pk>[0-9]+)$', views.PostDetails.as_view()),
    url(r'^comments/(?P<commentId>[0-9]+)/like$', views.LikeComment.as_view()),
    url(r'^comments$', views.CommentList.as_view()),
    url(r'^comments/(?P<pk>[0-9]+)$', views.CommentDetails.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
