from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Posts(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100, blank=False)
    content = models.TextField()
    author = models.ForeignKey(User)
    class Meta:
        ordering = ('-created',)


class Comments(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    content = models.TextField(null=False)
    author = models.ForeignKey(User, null=True)
    post = models.ForeignKey(Posts,null=False)

    class Meta:
        ordering = ('-created',)

class PostLikes(models.Model):
    user = models.ForeignKey(User)
    post = models.ForeignKey(Posts)

    class Meta:
        unique_together = (("user", "post"),)

class CommentLikes(models.Model):
    user = models.ForeignKey(User)
    comment = models.ForeignKey(Comments)

    class Meta:
        unique_together = (("user", "comment"),)