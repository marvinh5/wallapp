from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from rest_framework import status
from rest_framework.filters import DjangoFilterBackend
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from wall.models import Posts, Comments, PostLikes, CommentLikes
from wall.serializer import PostSerializer, CommentSerializer, PaginatedPostSerializer
from django.db.models import Q, Count
from django.db import IntegrityError


class PostDetails(APIView):

    def get_object(self, pk):
        try:
            return  Posts.objects.get(pk=pk)
        except Exception:
            raise Http404

    def get(self, request, pk, format=None):
        post= Posts.objects.filter(id=pk).annotate(likes=Count('postlikes'))
        serializer = PostSerializer(post, many=True)
        return Response(serializer.data)

    def put(self, request, pk, format=None):
        post = self.get_object(pk)
        serializer = PostSerializer(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status= status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        post = Posts.objects.filter(pk=pk, author=request.user.id)
        if(post):
            post.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)


class PostList(ListAPIView):
    queryset = Posts.objects.annotate(likes=Count('postlikes'))
    serializer_class = PostSerializer
    pagination_class = PaginatedPostSerializer
    paginate_by = 10
    paginate_by_param = 'page_size'
    max_paginate_by = 100

    def post(self, request, format=None):
        request.data["author"] = request.user.id
        serializer = PostSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class LikeComment(APIView):
    def get(self, request, commentId):
        try:
            comment = Comments.objects.get(pk=commentId)
            if comment:
                try:
                    CommentLikes.objects.create(user=request.user, comment=comment)
                except IntegrityError:
                    print('duplicated like')
                updated_comment = Comments.objects.filter(id=commentId).annotate(likes=Count('commentlikes'))
                serializer = CommentSerializer(updated_comment[0])
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                raise Http404
        except ObjectDoesNotExist:
            raise  Http404


class LikePost(APIView):
    def get(self, request, postId):
        try:
            post = Posts.objects.get(pk=postId)
            if post:
                try:
                    PostLikes.objects.create(user=request.user, post=post)
                except IntegrityError:
                    print('duplicated like')
                updated_post = Posts.objects.filter(id=postId).annotate(likes=Count('postlikes'))
                serializer = PostSerializer(updated_post[0])
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                raise Http404
        except ObjectDoesNotExist:
            raise Http404


class CommentDetails(APIView):

    def get_object(self, pk):
        try:
            return Comments.objects.get(pk=pk)
        except Exception:
            raise Http404

    def get(self, request, pk):
        comments = Comments.objects.filter(id=pk).annotate(likes=Count("commentlikes"))
        serializer = CommentSerializer(comments)
        return Response(serializer.data)

    def put(self, request, pk):
        comment= self.get_object(pk)
        serializer = CommentSerializer(comment, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        comment = Comments.objects.filter((Q(author=request.user.id) | Q(post__author=request.user.id)) & Q(pk=pk))
        if comment:
            comment.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)


class CommentList(ListAPIView):
    queryset = Comments.objects.annotate(likes=Count("commentlikes"))
    serializer_class = CommentSerializer
    filter_backends = (DjangoFilterBackend,)
    filter_fields = ('post',)


    def post(self, request, format=None):
        request.data["author"] = request.user.id
        serializer = CommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
