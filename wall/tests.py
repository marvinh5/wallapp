from rest_framework.test import APITestCase, force_authenticate
from rest_framework import status
from django.contrib.auth.models import User
from wall.models import Posts, Comments

# Create your tests here.

class PostTest(APITestCase):
    post_id_to_test = 1
    user_to_test = None
    post_id_to_delete= None

    def test_create_post(self):
        author = User.objects.create_user('authorUser', 'testmail@gmail.com', '1234')
        response = self.client.post('/posts',{
            'title':'sometitle',
            'content':'somecontent',
            'author':author.id}, format="json")
        force_authenticate(response, token=None)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg="response %s" % response)

    def test_get_posts(self):
        response = self.client.get('/posts')
        force_authenticate(response, token=None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_update_posts(self):
        author = User.objects.create_user('authorUser', 'testmail@gmail.com', '1234')
        post = Posts.objects.create(title="test_title", content="test content", author=author)
        response = self.client.put('/posts/%s' % self.post_id_to_test , {
            'title': 'testposttitle',
            'content':'testpostcontent',
            'author': post.id}, format="json")
        force_authenticate(response ,token=None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class CommentsTest(APITestCase):
    def test_comment_creation(self):
        author = User.objects.create_user('authorUser', 'testmail@gmail.com', '1234')
        post = Posts.objects.create(title="test_title", content="test content", author=author)
        response = self.client.post('/comments', {'content': 'test content', 'author': author.id, 'post': post.id})
        force_authenticate(response, token=None)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_comment_delete(self):
        author = User.objects.create_user('authorUser', 'testmail@gmail.com', '1234')
        post = Posts.objects.create(title="test_title", content="test content", author=author)
        comment = Comments.objects.create(content="test content", author=author, post=post)
        response = self.client.delete('/comments/%s' % comment.id)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_comment_update(self):
        author = User.objects.create_user('authorUser', 'testmail@gmail.com', '1234')
        post = Posts.objects.create(title="test_title", content="test content", author=author)
        comment = Comments.objects.create(content="test content", author=author, post=post)
        response = self.client.put('/comments/%s' % comment.id, {'content':'diferent content', 'author': author.id, 'post':post.id})
        self.assertEqual(response.status_code, status.HTTP_200_OK)

