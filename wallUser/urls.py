from django.conf.urls import url
from wallUser import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^user$', views.UserDetails.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
