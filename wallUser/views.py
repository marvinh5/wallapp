from rest_framework import status
from rest_framework.views import APIView
from django.contrib.auth.models import User
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from django.core.mail import send_mail
from wallUser.serializers import UserSerializer
# Create your views here.

class UserDetails(APIView):
    permission_classes = (AllowAny,)

    def post(self, request, format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            user = User.objects.create_user(
                request.data['username'],
                request.data['email'],
                request.data['password'],
                first_name=request.data['first_name'],
                last_name=request.data['last_name']
            )
            if user:
                    send_mail(
                        'Welcome To WallApp',
                        'We are Happy to Have you!',
                        'WallApp',
                        [user.email],
                        fail_silently=False)
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
            else:
                return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

