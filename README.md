# WallApp Api #

The WallApp is and app for publishing post in a wall, you can like the post and comment.

# Setting Up #


```
#!bash

pip install -r requirements.txt
```


# Posts #

## Retrieving Posts ##
```
#!bash
host/posts
```

```
#!json

{
 "count": 2,
 "next": "host/posts?page=3",
 "previous": "host/post?page=1",
 "results":[{
    "title": "",
    "author":"",
    "content":"",
    "likes":0
   }
 ]
}

```
## like a post ##



```
#!bash

host/posts/:id/like
```


# Comments #

## like a comment ##


```
#!bash

host/comments/:id/like
```